\section*{Syntax}

\begin{flalign*}
    &\text{Kinds} &\kappa &::= \star \mid \kappa \to \kappa \\[.5em]
    &\text{Types} &A &::= \alpha \mid 1 \mid A \to B \mid \forall \alpha :: \kappa. A \mid \lambda \alpha :: \kappa. A \mid A \ B \mid \Pi \ \rho_A \mid \Sigma \ \rho_A \\[.5em]
    &\text{Monotype} &\tau &::= \alpha \mid 1 \mid \tau \to \sigma \mid \lambda \alpha :: \kappa. \tau \mid \tau \ \sigma \mid \Pi \ \rho_\tau \mid \Sigma \ \rho_\tau \\[.5em]
    &\text{Rows} &\rho_X &::= \cdot \mid \ell : X :: \rho \\[.5em]
    &\text{Row Label} &\ell, C &\in \textsf{Label} \\[.5em]
    &\text{Terms} &e &::= x \mid \lambda x. e \mid e \ e \mid \lambda (x : A). e \mid () \mid (e : A) \mid \Lambda \alpha :: \kappa. e \\
    &&&\hspace*{5mm}\mid \textsf{let } x = e \textsf{ in } e \mid \textsf{let type } \alpha :: \kappa = A \textsf{ in } e\\
    &&&\hspace*{5mm}\mid \textsf{let rec } f : A = \lambda x. e \textsf{ in } e \\
    &&&\hspace*{5mm}\mid C \ f \mid \textsf{match } f \textsf{ with } \overline{p \rightarrow f} \\
    &&&\hspace*{5mm}\mid \{ \ \ell = f; \ldots; \ell = f \ \} \mid f.\ell \mid \{ \ f \textsf{ with } \ell = f \ \} \\ 
    &&&\hspace*{5mm}\mid \textsf{let module } X = M \textsf{ in } f \mid P.x \\[.5em]
    &\text{Bindings} &B &::= \textsf{let } x : A = f\\
    &&&\hspace*{5mm}\mid \textsf{type } \alpha :: \kappa = A\\
    &&&\hspace*{5mm}\mid\textsf{module } X = M \\[.5em]
    &\text{Module Expressions} &M &::= P.X \\
    &&&\hspace*{5mm}\mid X \\
    &&&\hspace*{5mm}\mid \textsf{struct } \overline{B} \textsf{ end} \\[.5em]
    &\text{Paths} &P &::= X \mid P.X \\[.5em]
    &\text{Declarations} &D &::= \textsf{val } x : A\\
    &&&\hspace*{5mm}\mid \textsf{type } \alpha :: \kappa = A\\
    &&&\hspace*{5mm}\mid \textsf{module } X : S\\[.5em]
    &\text{Signature} &S &::= \textsf{sig } \overline{D} \textsf{ end}\\[.5em]
    &\text{Contexts} &\Gamma &::= \cdot \mid \Gamma, x : A \mid \Gamma, \alpha :: \kappa \mid \Gamma, \alpha :: \kappa = A \mid \Gamma, \textsf{module } X : S\\
\end{flalign*}

\section*{Typing Rules}
\noindent$\boxed{\Gamma \textsf{ ctx}}$
\begin{mathpar}
    \inferrule*[]
        { }
        { \cdot \textsf{ ctx}}
    
    \inferrule*[]
        { \Gamma \textsf{ ctx} \\ \Gamma \vdash A :: \star \\ x \notin \dom \Gamma}
        { \Gamma, x : A \textsf{ ctx}}

    \inferrule*[]
        { \Gamma \textsf{ ctx} \\ \alpha \notin \dom \Gamma }
        { \Gamma, \alpha :: \kappa \textsf{ ctx} }

    \inferrule*[]
        { \Gamma \textsf{ ctx} \\ \alpha \notin \dom \Gamma \\ \Gamma \vdash A :: \kappa}
        { \Gamma, \alpha :: \kappa = A \textsf{ ctx}}

    \inferrule*[]
        { \Gamma \textsf{ ctx} \\ X \notin \dom \Gamma \\ \Gamma \vdash S \textsf{ sig}}
        { \Gamma, \textsf{module } X : S \textsf{ ctx}}
\end{mathpar}
\noindent$\boxed{\Gamma \vdash A :: \kappa}$
\begin{mathpar}
    \inferrule*[]
        {\alpha :: \kappa \in \Gamma}
        { \Gamma \vdash \alpha :: \kappa }

    \inferrule*[]
        { \alpha :: \kappa = A \in \Gamma } 
        { \Gamma \vdash \alpha :: \kappa}

    \inferrule*[]
        { }
        { \Gamma \vdash 1 :: \star }

    \inferrule*[]
        { \Gamma \vdash A :: \star \\ \Gamma \vdash B :: \star }
        { \Gamma \vdash A \to B :: \star }

    \inferrule*[]
        { \Gamma, \alpha :: \kappa \vdash A :: \star }
        { \Gamma \vdash \forall \alpha :: \kappa. A :: \star }

    \inferrule*[]
        { \Gamma, \alpha :: \kappa_1 \vdash A :: \kappa_2}
        { \Gamma \vdash \lambda \alpha :: \kappa_1. A :: \kappa_1 \to \kappa_2}

    \inferrule*[]
        { \Gamma \vdash A :: \kappa_1 \to \kappa_2 \\ \Gamma \vdash B :: \kappa_1 }
        { \Gamma \vdash A \ B :: \kappa_2 }

    \inferrule*[]
        { \Gamma \vdash \rho \textsf{ row}}
        { \Gamma \vdash \Pi \ \rho :: \star }

    \inferrule*[]
        { \Gamma \vdash \rho \textsf{ row}}
        { \Gamma \vdash \Sigma \ \rho :: \star }    
\end{mathpar}
\noindent$\boxed{\Gamma \vdash \rho \textsf{ row}}$
\begin{mathpar}
    \inferrule*[]
        { }
        { \Gamma \vdash \cdot \textsf{ row}}

    \inferrule*[]
        { \Gamma \vdash A :: \star \\ \ell \notin \dom \rho \\ \Gamma \vdash \rho \textsf{ row}}
        { \Gamma \vdash \ell : A :: \rho \textsf{ row}}
\end{mathpar}

\noindent$\boxed{\Gamma \vdash P \rhd S}$
\begin{mathpar}
    \inferrule*[]
        { \textsf{module } X : S \in \Gamma }
        { \Gamma \vdash X \rhd S }
    
    \inferrule*[]
        { \Gamma \vdash P \rhd \textsf{sig } \overline{D} \textsf{ end} \\ \textsf{module } X : S \in \overline{D}}
        { \Gamma \vdash P.X \rhd S }
\end{mathpar}
\noindent$\boxed{\Gamma \vdash S \textsf{ sig}}$
\begin{mathpar}
    \inferrule*[]
        {  }
        { \Gamma \vdash (\textsf{sig} \textsf{ end}) \textsf{ sig}}

    \inferrule*[]
        { \Gamma \vdash D \textsf{ decl} \\ \Gamma, D \vdash (\textsf{sig } \overline{D} \textsf{ end}) \textsf{ sig}}
        { \Gamma \vdash (\textsf{sig } (D, \overline{D}) \textsf{ end}) \textsf{ sig}}
\end{mathpar}
\noindent$\boxed{\Gamma \vdash D \textsf{ decl}}$
\begin{mathpar}
    \inferrule*[]
        { \Gamma \vdash A :: \star}
        { \Gamma \vdash (\textsf{val } x : A) \textsf{ decl}}

    \inferrule*[]
        { \Gamma \vdash A :: \kappa}
        { \Gamma \vdash (\textsf{type } \alpha :: \kappa = A) \textsf{ decl}}

    \inferrule*[]
        { \Gamma \vdash S \textsf{ sig} }
        { \Gamma \vdash (\textsf{module } X : S) \textsf{ decl} }
\end{mathpar}


{\setlength{\mathindent}{0cm}
\begin{empheq}[box=\fbox]{align*}
    &\Gamma \vdash A \leadsto A'\\
    &\Gamma \vdash \rho \leadsto \rho'
\end{empheq}}
\begin{mathpar}
    \inferrule*[]
        { }
        { \Gamma[\alpha :: \kappa] \vdash \alpha \leadsto \alpha }


    \inferrule*[]
        { }
        { \Gamma[\alpha :: \kappa = A] \vdash \alpha \leadsto A }

    \inferrule*[]
        { }
        { \Gamma \vdash 1 \leadsto 1 }

    \inferrule*[]
        { \Gamma, \alpha :: \kappa \vdash A \leadsto A' }
        { \Gamma \vdash \forall \alpha :: \kappa. A \leadsto \forall \alpha :: \kappa. A'}

    \inferrule*[]
        { \Gamma, \alpha :: \kappa \vdash A \leadsto A' }
        { \Gamma \vdash \lambda \alpha :: \kappa. A \leadsto \lambda \alpha :: \kappa. A' }

    \inferrule*[]
        { \Gamma \vdash A \leadsto A'  }
        { \Gamma \vdash A \to B \leadsto A' \to B }


    \inferrule*[]
        { \Gamma \vdash B \leadsto B'  }
        { \Gamma \vdash A \to B \leadsto A \to B' }

    \inferrule*[]
        { \Gamma \vdash A \leadsto A'  }
        { \Gamma \vdash A \ B \leadsto A' \ B }


    \inferrule*[]
        { \Gamma \vdash B \leadsto B'  }
        { \Gamma \vdash A \ B \leadsto A \ B' }

    \inferrule*[]
        { }
        { \Gamma \vdash (\lambda \alpha :: \kappa. A) \ B \leadsto A[\alpha := B] }

    \inferrule*[]
        { \Gamma \vdash \rho \leadsto \rho' }
        { \Gamma \vdash \Pi \ \rho \leadsto \Pi \ \rho' }

    \inferrule*[]
        { \Gamma \vdash \rho \leadsto \rho' }
        { \Gamma \vdash \Sigma \ \rho \leadsto \Sigma \ \rho' }    
    \\

    \inferrule*[]
        { \Gamma \vdash A \leadsto A'}
        { \Gamma \vdash \ell : A :: \rho \leadsto \ell : A' :: \rho }

    \inferrule*[]
        { \Gamma \vdash \rho \leadsto \rho' }
        { \Gamma \vdash \ell : A :: \rho \leadsto \ell : A :: \rho' }

    \inferrule*[]
        { }
        { \Gamma \vdash \cdot \leadsto \cdot }
\end{mathpar}

\noindent$\boxed{\Gamma \vdash e \Rightarrow A \leadsto f}$
\begin{mathpar}
    \inferrule*[]
        { x : A \in \Gamma }
        { \Gamma \vdash x \Rightarrow A \leadsto x }

    \inferrule*[]
        { \Gamma \vdash e_1 \Rightarrow A \leadsto f_1 \\ \Gamma \vdash A \bullet e_2 \Rightarrow C \leadsto \mathbb{T}, f_2 }
        { \Gamma \vdash e_1 \ e_2 \Rightarrow C \leadsto \mathbb{T}[f_1] \ f_2 }

    \inferrule*[]
        { \Gamma \vdash e \Leftarrow A \leadsto f }
        { \Gamma \vdash (e : A) \Rightarrow A \leadsto f }

    \inferrule*[]
        { \Gamma \vdash \tau :: \star \\ \Gamma, x : \tau \vdash e \Rightarrow B \leadsto f }
        { \Gamma \vdash \lambda x. e \Rightarrow \tau \to B \leadsto \lambda x : \tau. f }

    \inferrule*[]
        { }
        { \Gamma \vdash () \Rightarrow 1 \leadsto () }
    
    \inferrule*[]
        { \Gamma \vdash A :: \star \\ \Gamma, x : A \vdash e \Rightarrow B \leadsto f }
        { \Gamma \vdash \lambda (x : A).e \Rightarrow A \to B \leadsto \lambda x : A. f }

    \inferrule*[]
        { \Gamma, \alpha :: \kappa \vdash e \Rightarrow A \leadsto f }
        { \Gamma \vdash \Lambda \alpha :: \kappa. e \Rightarrow \forall \alpha :: \kappa. A \leadsto \Lambda \alpha :: \kappa. f}

    \inferrule*[]
        { \Gamma \vdash e_1 \Rightarrow A \leadsto f_1 \\ \Gamma, x : A \vdash e_2 \Rightarrow B \leadsto f_2 }
        { \Gamma \vdash \textsf{let } x = e_1 \textsf{ in } e_2 \Rightarrow B \leadsto \textsf{let } x : A = f_1 \textsf{ in } f_2}

    \inferrule*[]
        { \Gamma \vdash A :: \kappa \\ \Gamma, \alpha :: \kappa = A \vdash e \Rightarrow B \leadsto f }
        { \Gamma \vdash \textsf{let type } \alpha :: \kappa = A \textsf{ in } e \Rightarrow B \leadsto f }

    \inferrule*[]
        { \Gamma \vdash A :: \star \\ \Gamma, f : A \vdash \lambda x. e_1 \Leftarrow A \leadsto f_1 \\ \Gamma, f : A \vdash e_2 \Rightarrow B \leadsto f_2}
        { \Gamma \vdash \textsf{let rec } f : A = \lambda x. e_1 \textsf{ in } e_2 \Rightarrow B \leadsto \textsf{let rec } f : A = f_1 \textsf{ in } f_2}

    \inferrule*[]
        { \Gamma \vdash e \Rightarrow \Pi \ \rho \leadsto f \\ \ell : A \in \rho }
        { \Gamma \vdash e.\ell \Rightarrow A \leadsto f.\ell }

    \inferrule*[]
        { \forall 1 \leq i \leq n. \ \Gamma \vdash e_i \Rightarrow A_i \leadsto f_i }
        { \Gamma \vdash \{ \ \ell_1 = e_1; \ldots; \ell_n = e_n \ \} \Rightarrow \Pi \ ( \ \ell_1 : A_1 :: \cdots :: \ell_n : A_n \ ) \leadsto \{ \ \ell_1 = f_1; \ldots; \ell_n = f_n \ \} }

    \inferrule*[]
        { \Gamma \vdash e_1 \Rightarrow \Pi \ \rho \leadsto f_1 \\  \ell : A \in \rho \\ \Gamma \vdash e_2 \Leftarrow A \leadsto f_2 }
        { \Gamma \vdash \{ \ e_1 \textsf{ with } \ell = e_2 \ \} \Rightarrow \Pi \ \rho \leadsto \{ \ f_1 \textsf{ with } \ell = f_2 \ \} }
\end{mathpar}
\begin{mathpar}
    \inferrule*[]
        { \Gamma \vdash \Gamma(C) \leq \Sigma \ \rho \\ C : B \in \rho \\ \Gamma \vdash e \Leftarrow B \leadsto f }
        { \Gamma \vdash C \ e \Rightarrow \Sigma \ \rho \leadsto (C \ f : \Sigma \ \rho) }

    \inferrule*[]
        { \Gamma \vdash e \Rightarrow \Sigma \ \rho \leadsto f \\ \Gamma \vdash \tau :: \star \\ \forall 1 \leq i \leq n. \ \Gamma \vdash p_i \to e_i \Leftarrow \Sigma \ \rho \to \tau \leadsto q_i \to f_i}
        { \Gamma \vdash \textsf{match } e \textsf{ with } \overline{p_i \to e_i} \Rightarrow \tau \leadsto \textsf{match } f \textsf{ with } \overline{q_i \to f_i}}

    \inferrule*[]
        { \Gamma \vdash M \Rightarrow S \leadsto M' \\ \Gamma, \textsf{module } X : S \vdash e \Rightarrow A \leadsto f }
        { \Gamma \vdash \textsf{let module } X = M \textsf{ in } e \Rightarrow A \leadsto \textsf{let module } X : S = M' \textsf{ in } f}

    \inferrule*[]
        { \Gamma \vdash P \rhd \textsf{sig } \overline{D} \textsf{ end} \\ (\textsf{val } x : A) \in \overline{D}}
        { \Gamma \vdash P.x \Rightarrow A \leadsto P.x}
\end{mathpar}
\noindent$\boxed{\Gamma \vdash e \Leftarrow A \leadsto f}$
\begin{mathpar}
    \inferrule*[]
        {  }
        { \Gamma \vdash () \Leftarrow 1 \leadsto () }

    \inferrule*[]
        { \Gamma, x : A \vdash e \Leftarrow B \leadsto f }
        { \Gamma \vdash \lambda x. e \Leftarrow A \to B \leadsto \lambda x : A. f }

    \inferrule*[]
        { \Gamma \vdash e \Rightarrow A \leadsto f \\ \Gamma \vdash A \leq B \leadsto \mathbb{T}}
        { \Gamma \vdash e \Leftarrow B \leadsto \mathbb{T}[f]}

    \inferrule*[]
        { \Gamma, \alpha :: \kappa \vdash e \Leftarrow A \leadsto f }
        { \Gamma \vdash e \Leftarrow \forall \alpha :: \kappa. A \leadsto \Lambda \alpha :: \kappa. f }

    \inferrule*[]
        { \Gamma \vdash A \leadsto^* B \\ \Gamma \vdash e \Leftarrow B \leadsto f }
        { \Gamma \vdash e \Leftarrow A \leadsto f }

    \inferrule*[]
        { \forall 1 \leq i \leq n. \ \Gamma \vdash e_i \Leftarrow A_i \leadsto f_i }
        { \Gamma \vdash \{ \ \ell_1 = e_1; \ldots; \ell_n = e_n \ \} \Leftarrow \Pi \ ( \ \ell_1 : A_1 :: \cdots :: \ell_n : A_n \ ) \leadsto \{ \ \ell_1 = f_1; \ldots; \ell_n = f_n \ \} }

    \inferrule*[]
        { \Gamma \vdash e_1 \Leftarrow \Pi \ \rho \leadsto f_1 \\  \ell : A \in \rho \\ \Gamma \vdash e_2 \Leftarrow A \leadsto f_2 }
        { \Gamma \vdash \{ \ e_1 \textsf{ with } \ell = e_2 \ \} \Leftarrow \Pi \ \rho \leadsto \{ \ f_1 \textsf{ with } \ell = f_2 \ \} }

    \inferrule*[]
        { C : A \in \rho \\ \Gamma \vdash e \Leftarrow A \leadsto f }
        { \Gamma \vdash C \ e \Leftarrow \Sigma \ \rho \leadsto (C \ f : \Sigma \ \rho) }
    
    \inferrule*[]
        { \Gamma \vdash e \Rightarrow \Sigma \ \rho \leadsto f \\ \forall 1 \leq i \leq n. \ \Gamma \vdash p_i \to e_i \Leftarrow \Sigma \ \rho \to A \leadsto q_i \to f_i}
        { \Gamma \vdash \textsf{match } e \textsf{ with } \overline{p_i \to e_i} \Leftarrow A \leadsto \textsf{match } f \textsf{ with } \overline{q_i \to f_i}}
\end{mathpar}
\noindent$\boxed{\Gamma \vdash A \bullet e \Rightarrow C \leadsto \mathbb{T}, f}$
\begin{mathpar}
    \inferrule*[]
        {  \Gamma \vdash \tau :: \kappa \\ \Gamma \vdash A[\alpha := \tau] \bullet e \Rightarrow C \leadsto \mathbb{T}, f }
        { \Gamma \vdash \forall \alpha :: \kappa. A \bullet e \Rightarrow C \leadsto \mathbb{T}[([\cdot] \ [\tau])], f }

    \inferrule*[]
        { \Gamma \vdash e \Leftarrow A \leadsto f }
        { \Gamma \vdash A \to C \bullet e \Rightarrow C \leadsto [\cdot], f }

    \inferrule*[]
        { \Gamma \vdash A \leadsto^* B \\ \Gamma \vdash B \bullet e \Rightarrow C \leadsto \mathbb{T}, f}
        { \Gamma \vdash A \bullet e \Rightarrow C \leadsto \mathbb{T}, f}
\end{mathpar}
\noindent$\boxed{\Gamma \vdash M \Rightarrow S \leadsto M}$
\begin{mathpar}
    \inferrule*[]
        { \Gamma \vdash P \rhd \textsf{sig } \overline{D} \textsf{ end} \\ (\textsf{module } X : S) \in \overline{D}}
        { \Gamma \vdash P.X \Rightarrow S \leadsto P.X }

    \inferrule*[]
        { \textsf{module } X : S \in \Gamma }
        { \Gamma \vdash X \Rightarrow S \leadsto X }

    \inferrule*[]
        { \forall 1 \leq i \leq n. \ \Gamma \vdash B_i \Rightarrow D_i \leadsto B_i'}
        { \Gamma \vdash \textsf{struct } \overline{B} \textsf{ end} \Rightarrow \textsf{sig } \overline{D} \textsf{ end} \leadsto \textsf{struct } \overline{B}' \textsf{ enc}}
\end{mathpar}
\noindent$\boxed{\Gamma \vdash B \Rightarrow D \leadsto B'}$
\begin{mathpar}
    \inferrule*[]
        { \Gamma \vdash e \Rightarrow A \leadsto f }
        { \Gamma \vdash \textsf{let } x = e \Rightarrow (\textsf{val } x : A) \leadsto \textsf{let } x : A = f}

    \inferrule*[]
        { \Gamma \vdash A :: \kappa \\ A \leadsto^* A'}
        { \Gamma \vdash \textsf{type } \alpha :: \kappa = A \Rightarrow (\textsf{type } \alpha :: \kappa = A') \leadsto \textsf{type } \alpha :: \kappa = A'}
    
    \inferrule*[]
        { \Gamma \vdash M \Rightarrow S \leadsto M' }
        { \Gamma \vdash \textsf{module } X = M \Rightarrow (\textsf{module } X : S) \leadsto \textsf{module } X = M'}
\end{mathpar}


\noindent$\boxed{\Gamma \vdash A \leq B \leadsto \mathbb{T}}$
\begin{mathpar}
   \inferrule*[]
        { \Gamma \vdash A =_\beta B \\ \Gamma \vdash A, B :: \kappa }
        { \Gamma \vdash A \leq B \leadsto [\cdot] }

    \inferrule*[]
        { \Gamma \vdash A \leq B \leadsto \mathbb{T}_1 \\ \Gamma \vdash B \leq C \leadsto \mathbb{T}_2 }
        { \Gamma \vdash A \leq C \leadsto \mathbb{T}_2[\mathbb{T}_1[\cdot]] }
        
    % Higher order subtyping was being inconsistent. TODO: Figure this out later
    % \inferrule*[]
    %     { \Gamma \vdash A \leq B }
    %     { \Gamma \vdash A \ C \leq B \ C \leadsto ([\cdot] : A \ C ) }

    % \inferrule*[]
    %     { \Gamma, \alpha :: \kappa \vdash A \leq B  }
    %     { \Gamma \vdash \lambda \alpha :: \kappa. A \leq \lambda \alpha :: \kappa. B }

    \inferrule*[]
        { \Gamma \vdash B_1 \leq A_1 \leadsto \mathbb{T}_1 \\ \Gamma \vdash A_2 \leq B_2 \leadsto \mathbb{T}_2 }
        { \Gamma \vdash A_1 \to A_2 \leq B_1 \to B_2 \leadsto \lambda x : B_1. \mathbb{T}_2[([\cdot] \ (\mathbb{T}_1[x]))] }

    \inferrule*[]
        { \Gamma, \alpha :: \kappa \vdash A \leq B \leadsto \mathbb{T}  }
        { \Gamma \vdash A \leq \forall \alpha :: \kappa. B \leadsto \Lambda \alpha :: \kappa. \mathbb{T} }

    \inferrule*[]
        { \Gamma \vdash \tau :: \kappa \\ \Gamma \vdash A[\alpha := \tau] \leq B \leadsto \mathbb{T}}
        { \Gamma \vdash \forall \alpha :: \kappa. A \leq B \leadsto \mathbb{T}[([\cdot] \ [\tau])]}
\end{mathpar}





